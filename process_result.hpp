/*
 * Copyright 2019 Xilinx Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define ALARM_ADDRESS "192.168.1.11"
#pragma once
#include <opencv2/imgcodecs.hpp>
#include "./falldetect.hpp"
using namespace cv;
using namespace std;
using Result = vitis::ai::OpenPoseResult::PosePoint;

static cv::Mat process_result(cv::Mat &image,
                              vitis::ai::OpenPoseResult results,
                              bool is_jpeg) {
  static int fall_num = 0;
  static bool alarm = false;
  int x1,x2,y1,y2; 
  int red = 255;

  static cv::VideoWriter video;  
  
  std::string fileName = "./web/fall.jpg";
    
  //  Array libseq  0 = ead                 
  vector<vector<int>> limbSeq = {{0, 1},  {1, 2},   {2, 3},  {3, 4}, {1, 5},
                                 {5, 6},  {6, 7},   {1, 8},  {8, 9}, {9, 10},
                                 {1, 11}, {11, 12}, {12, 13}};
  for (size_t k = 1; k < results.poses.size(); ++k) {
    for (size_t i = 0; i < results.poses[k].size(); ++i) {
      if (results.poses[k][i].type == 1) {
        cv::circle(image, results.poses[k][i].point, 5, cv::Scalar(0, 255, 0),
                   -1);
      }
    }
    for (size_t i = 0; i < limbSeq.size(); ++i) {
      Result a = results.poses[k][limbSeq[i][0]];
      Result b = results.poses[k][limbSeq[i][1]];
      if (a.type == 1 && b.type == 1) 
      {
        red = 10;
        if ((i == 0) || (i == 7) || (i==10))
        {
            x1 = a.point.x;
            y1 = a.point.y;
            x2 = b.point.x;
            y2 = b.point.y;
            if (fall_detect (k,i,x1,y1,x2,y2))
            {  
               cv::putText(image, std::string("Fall Detcted at ") + std::to_string(k),
               cv::Point(10, 50), cv::FONT_HERSHEY_SIMPLEX, 0.5,
               cv::Scalar(20, 20, 180), 2, 1);
               if (!alarm)  // To Trigger only once
               {
                std::string command = "curl -X POST https://maker.ifttt.com/trigger/FallDetect/with/key/<iftttkey>";
                system(command.c_str());
                command = "curl -X POST http://"+std::string (ALARM_ADDRESS)+ std::string ("/SirenOn");
                system(command.c_str());
                alarm = true; 
               }           
            }
            red = 255;
        }
        cv::line(image, a.point, b.point, cv::Scalar(red, 255, 255), 3, 4);
      }
    }
  }
//----------------------------------------

  /* Video  .avi use MJPG work
      mp4 use mp4v, MP4V, XVID, H264 did not work
      webm use  vp80, HEVC  cannot create file
      webm use  H264 error with OMX handler
      webm use  VP80 create 0 bytes webm file, 
      avi use mjpg,HEVC did not work ** case sensive **
      If you use Size in VideoWriter not match. You will get garbage video
      Width x Height get from  Write to image and check the size of image.
  */
  if (alarm && fall_num == 0)  //Record picture when first fall detect
  {
    cv::imwrite(fileName, image);
    int codec = cv::VideoWriter::fourcc('M','J','P','G');
    double fps  = 10.0;
    video = cv::VideoWriter("./web/webcam.avi",codec,fps,cv::Size(2304,1536),true);
    fall_num++;
  }
  if (fall_num > 0)
  {
      video.write(image); 
  }
  
 //---------------------------------------- 
  return image;
}

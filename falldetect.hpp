/* To Check Fall detection by checking trend of changing Y Coordinate */

#include <iostream>
#include <math.h>       /* atan */

#define PI 3.14159265
#define FALL_TIME_REFERENCE  5   // Trial and Error at this poin
#define HEAD_SEQ      0
#define RIGHT_SEQ 7
#define LEFT_SEQ  10
#define LAST_SEQ  12
#define NO_OF_SEQ 13
#define NO_OF_CHK  3        // Check 3 points
#define REFERENCE_ANGLE 90.0
#define FALL_ANGLE_THRESHOLD  45.0  // Degree for Fall
#define FALL_ANGLE_START  20.0 // Degree  to start detect



/* Fall detect to check falling by calculate change angle speed of segment head, upbody left and 
right
Input : k  no of person to check
        i  sequence of limb  (we intrest only 0, 8 and 12)
        x1,y1 coordinate of staring point of limb sequence
        x2,y2 coordinate of end point of limb sequence

Detect calculate from two parameters
    1) Change angle of head, upper right leg and lower left leg  < 40 Degre?
    2) Speed of angle change. If change slowly maybe they lay down to sleep, sit
Fall detection must avoid these false alarms
    - Sleeping 
    - Sit
    - 


*/
int fall_detect (int k, int i,int x1, int y1,int x2, int y2)
{
  static double prev_angle[NO_OF_SEQ] = {90.0,90.0,90.0,90.0,90.0,90.0,90.0,90.0,90.0,90.0,
                                  90.0,90.0,90.0};
  static long int ref_time = 0, start_time = 0;
  static int change[3] = {0,0,0};
  static bool trigger = false;
  double result, diff_angle;
  bool fall = false;
  
  int sum = 0;
  // Interest only  limbseq 0 (head), 7 (upper left), 10 (lower right)
  

  if (i == HEAD_SEQ)   //Clear value in change for staring point
  {
    change[0] = 0;
    change[1] = 0;
    change[2] = 0;
  }
    
  if ((i== HEAD_SEQ) || (i== RIGHT_SEQ) || (i== LEFT_SEQ))
  { 
    if (y2 != y1)
    {
       result = (atan2((y2-y1),(x2-x1)) * 180) / PI;
    }
    else
    {
      result = 90.0;
    }
 //   diff_angle = abs(prev_angle[i] - result);
    diff_angle = abs(REFERENCE_ANGLE - result);
    if ((diff_angle >  FALL_ANGLE_START) && !trigger)
    {
          start_time = ref_time;
          trigger = true;
    }
    if (diff_angle < FALL_ANGLE_START)
          trigger = false;     //Reset
    
    if (diff_angle > FALL_ANGLE_THRESHOLD)
    {
      switch (i)
      {
        case HEAD_SEQ:   
            change[0] = 1;
            break;
        case RIGHT_SEQ:
            change[1] = 1;
            break;
        case LEFT_SEQ:
            change[2] = 1;
            for (int j = 0; j < NO_OF_CHK ; j++)
                sum  += change[j];
            ref_time++;    // Increase time when end of each person
            break;
      }  
    }
    if (sum > 2)
    {
      long int fall_time  = 0;
      if ((ref_time - start_time)  < FALL_TIME_REFERENCE)
          fall = true;
    }
    
    prev_angle[i] = result;
    std::cout << "Fall Time : " << (ref_time - start_time) << "  Limb :  " << i  << " Diff Degree : "  <<  diff_angle  
     << "   Sum : " << sum << "\r" << std::endl;

  }
  return fall;
}

